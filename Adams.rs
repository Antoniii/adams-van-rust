// rustc Adams.rs && ./Adams

use std::f64;

fn main() {
    const N: usize = 13;
    let h = 0.1;
    
    let mut x = 1.0;
    let mut t = 0.0;
    let mut f = [0.0; N + 1]; // N + 1 = 14

    for i in 1..N+1 {
        let f1 = func(t, x);
        let f2 = func(t + h, x + h*f1);
        f[i] = x + h*(f1 + f2)/2.0;
        t += h;
        //println!("f[{}] = {}", i, f[i]);
    }
    
    t = 0.;   

    for i in 3..N {
        let x_predict = x + h/24. * (55.*f[i] - 59.*f[i-1] + 37.*f[i-2] - 9.*f[i-3]);
        t += h;
        
        f[i+1] = func(t, x_predict);
        x = x + h/24. * (9.*f[i+1] + 19.*f[i] - 5.*f[i-1] + f[i-2]);
        println!("t = {:.2}, x = {:.4}", t, x);
    }
 	
    t = 0.;
    print!("\n");

    for _i in 3..N {
        t += h;
        x = fun_solve(t);
        println!("t_acc = {:.1}, x_acc = {:.4}", t, x);
    }
}

fn func(t: f64, x: f64) -> f64 {
    return 2.0 * (t*t + x);
}

fn fun_solve(t: f64) -> f64 {
    return 1.5 * f64::exp(2.*t) - t.powf(2.) - t - 0.5;
}

