### curl https://sh.rustup.rs -sSf | sh  
### source $HOME/.cargo/env  
### rustc ––version  

## rustc Adams.rs && ./Adams  

![](https://gitlab.com/Antoniii/adams-van-rust/-/raw/main/photo_2023-06-06_09-23-42.jpg)

t = 0.10, x = 1.1657  
t = 0.20, x = 1.4509  
t = 0.30, x = 1.7954  
t = 0.40, x = 2.2042  
t = 0.50, x = 2.7372  
t = 0.60, x = 3.4176  
t = 0.70, x = 4.2651  
t = 0.80, x = 5.3306  
t = 0.90, x = 6.6723  
t = 1.00, x = 8.3495  
  
t_acc = 0.1, x_acc = 1.2221  
t_acc = 0.2, x_acc = 1.4977  
t_acc = 0.3, x_acc = 1.8432  
t_acc = 0.4, x_acc = 2.2783  
t_acc = 0.5, x_acc = 2.8274  
t_acc = 0.6, x_acc = 3.5202  
t_acc = 0.7, x_acc = 4.3928  
t_acc = 0.8, x_acc = 5.4895  
t_acc = 0.9, x_acc = 6.8645  
t_acc = 1.0, x_acc = 8.5836  
